﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AGLProgrammingChallenge.WebUI.Models;
using AGLProgrammingChallenge.Services;

namespace AGLProgrammingChallenge.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly AglClientService _aglCLient;
        public HomeController(AglClientService aglClient)
        {
            _aglCLient = aglClient;
        }
        public async Task<IActionResult> Index()
        {
            var owners = await _aglCLient.GetCatsByGender();
            return View(owners);
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
