﻿using System.Collections.Generic;

namespace AGLProgrammingChallenge.Model
{
    public class OwnerGenderGroup
    {
        public string Gender { get; set; }
        public List<Owner> Owner { get; set; }
    }
}
