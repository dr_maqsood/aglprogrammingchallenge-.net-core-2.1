using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using AGLProgrammingChallenge.Services;
using AGLProgrammingChallenge.WebUI;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace AGLProgrammingChallenge.Test
{
    public class PersonUnitTest : IClassFixture<WebApplicationFactory<Program>>
    {
        private HttpClient Client;
        public PersonUnitTest(WebApplicationFactory<Program> factory)
        {
            Client = factory.CreateClient();
        }
        [Fact]
        public async void TestClient()
        {
            var result = await Client.GetAsync("/");
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }
        [Fact]
        public async void Client_Get_Cats_By_Gender()
        {
            var builder = new ConfigurationBuilder().AddInMemoryCollection(new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>( "AglServiceBaseUrl", "http://agl-developer-test.azurewebsites.net/"),
                new KeyValuePair<string, string>("AGLServicePersonServiceEndpoint", "/people.json")
            });

            IConfiguration config = builder.Build();

            Client.BaseAddress = new System.Uri( config["AglServiceBaseUrl"]);
            var aglClientService = new AglClientService(Client, config);
            var gender = await aglClientService.GetCatsByGender();
            
            Assert.True(gender != null);
        }
    }
}
