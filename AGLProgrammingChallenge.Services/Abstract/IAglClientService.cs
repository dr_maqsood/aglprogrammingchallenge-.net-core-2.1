﻿using AGLProgrammingChallenge.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AGLProgrammingChallenge.Services
{
    public interface IAglClientService
    {
        /// <summary>
        /// Get all owner from service
        /// </summary>
        /// <returns></returns>
        Task<IList<Owner>> GetOwners();
        /// <summary>
        /// Get cats by gender
        /// </summary>
        /// <returns>return list of cats by gender</returns>
        Task<List<OwnerGenderGroup>> GetCatsByGender();
    }
}
