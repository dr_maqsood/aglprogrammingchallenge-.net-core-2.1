﻿using AGLProgrammingChallenge.Model;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AGLProgrammingChallenge.Services
{
    public class AglClientService : IAglClientService
    {
        private readonly HttpClient _httpClient;
        private IConfiguration _configuration;
        public AglClientService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
        }
        /// <summary>
        /// Get all owner from service
        /// </summary>
        /// <returns></returns>
        public async Task<IList<Owner>> GetOwners()
        {
            var result = await _httpClient.GetAsync(_configuration["AGLServicePersonServiceEndpoint"]);
            result.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<List<Owner>>(await result.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// Get cats by gender
        /// </summary>
        /// <returns>return list of cats by gender</returns>
        public async Task<List<OwnerGenderGroup>> GetCatsByGender()
        {
            List<OwnerGenderGroup> ownerByGenderList = new List<OwnerGenderGroup>();
            try
            {
                var ownerList = await GetOwners();

                ownerByGenderList = ownerList.ToList()
                    .GroupBy(z => z.Gender)
                    .Select(k => new OwnerGenderGroup
                    {
                        Gender = k.Key,
                        Owner = k.Select(i => new Owner
                        {
                            Name = i.Name,
                            Age = i.Age,
                            Gender = i.Gender,
                            Pets = i.Pets?.Where(x => x.Type == "Cat").ToList(),
                        }).ToList()
                    })
                    .ToList();
            }
            catch (Exception ex)
            {
                //TODO: Add logging
            }
            return ownerByGenderList;
        }
    }
}
