# README #

This README covers steps which are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

      A solution to AGL Programming Challenge (http://agl-developer-test.azurewebsites.net/)

* Version 2.1 (re-written from the scratch in Microsoft ASP.Net MVC using .Net Core 2.1)


### How do I get set up? ###

* Summary of set up (Required Visual Studio 2017 15.8.0 with ASP.Net MVC Core 2.1 components)
* Dependencies (Please see Package file, nuget should resolve all dependencies)
* How to run tests (User xUnit test runner explorer)
* Run/Debug from Visual Studio and it will open the default web page


### Who do I talk to? ###

* Repo owner or admin (Please send email to dr_maq@hotmail.com for any further improvements)